<?php
/**
 * Created by PhpStorm.
 * User: ADRIAN
 * Date: 19/03/2018
 * Time: 23:30
 */

namespace AppBundle\Entity;
use Doctrine\ORM\EntityRepository;
use \Doctrine\ORM\Query;

class DriverRepository extends EntityRepository
{

    /**
     * @param $id
     * @param $deliveryDate
     * @return devuelve 0 en caso de no encontrar resultados. Devuelve el transportista en caso de encontrarlo
     *
     */
    public function findDriversByIdAndOrderDate($id, $deliveryDate)
    {
        $date = date("Y-m-d", strtotime($deliveryDate));
        $qb =  $this->createQueryBuilder('d');
        $qb->select('d, o')
        ->join('d.orders', 'o')
        ->where('d.id = :id AND o.sentDate = :deliveryDate')
            ->setParameters(['id' => $id, 'deliveryDate' => $date]);

        $results  = $qb->getQuery()->getArrayResult();
        if($results) return $results[0];
        else return 0;
    }

    public function getAllDriversById()
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('d');
        $qb->from('AppBundle:Driver', 'd', 'd.id');
        return $qb->getQuery()->getResult();
    }

}