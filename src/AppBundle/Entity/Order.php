<?php

namespace AppBundle\Entity;
use JsonSerializable;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Driver;

/**
 * Order
 * @ORM\Entity(repositoryClass="DictionaryBundle\Entity\OrderRepository")
 * @ORM\Table(name="orders")
 * @ORM\HasLifecycleCallbacks()
 */
class Order implements JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="full_name", type="string")     
     */
    private $fullName;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", unique=true)     
     */
    private $email;

      /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string")
     */
    private $phone;

      /**
     * @var string
     *
     * @ORM\Column(name="address", type="string")
     */
    private $address;

    /**
     * @ORM\Column(type="date")
     */
    private $sentDate;

 	/**
     * @var string
     *
     * @ORM\Column(name="delivery_range", type="string")
     */
    private $deliveryRange;

    /**
     * @ORM\ManyToOne(targetEntity="Driver")
     * @ORM\JoinColumn(name="driver_id", referencedColumnName="id")
     */
    private $driver;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fullName
     *
     * @param string $fullName
     *
     * @return Order
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Get fullName
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Order
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Order
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Order
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set sentDate
     *
     * @param \DateTime $sentDate
     *
     * @return Order
     */
    public function setSentDate($sentDate)
    {
        $this->sentDate = $sentDate;

        return $this;
    }

    /**
     * Get sentDate
     *
     * @return \DateTime
     */
    public function getSentDate()
    {
        return $this->sentDate;
    }

    /**
     * Set deliveryRange
     *
     * @param string $deliveryRange
     *
     * @return Order
     */
    public function setDeliveryRange($deliveryRange)
    {
        $this->deliveryRange = $deliveryRange;

        return $this;
    }

    /**
     * Get deliveryRange
     *
     * @return string
     */
    public function getDeliveryRange()
    {
        return $this->deliveryRange;
    }


    /**
     * Set driver
     *
     * @param \AppBundle\Entity\Driver $driver
     *
     * @return Order
     */
    public function setDriver(\AppBundle\Entity\Driver $driver = null)
    {
        $this->driver = $driver;

        return $this;
    }

    /**
     * Get driver
     *
     * @return \AppBundle\Entity\Driver
     */
    public function getDriver()
    {
        return $this->driver;
    }

    public function jsonSerialize()
    {
        return array(
            'fullName' => $this->fullName,
            'email'=> $this->email,
            'phone'=> $this->phone,
            'address' => $this->sentDate,
            'deliveryRange' => $this->deliveryRange
        );
    }


}
