<?php

namespace AppBundle\Controller;


use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Order;

class OrderController extends Controller
{
    /**
     * @Route("/order/create", name="create_order")
     * @param Request $request
     * @return Response Response JSON con:
     *          status que será 1 o 0 en función del éxito de la operación
     *          Message que da información adicional sobre la inserción
     */
    public function createOrder(Request $request)
    {
    	$order = $this->getPostOrder($request);
    	//var_dump($order);
        $response = new Response();
    	//En caso de que falle algún campo, se indica el error
    	if(in_array(null, $order, true))
    	{
            $resp = array("status" => 0,
                "Message" => "Introduzca los datos correctamente  ");
            $response->setContent(json_encode($resp));
            $response->setStatusCode(500);
    	}
    	else
    	{
            $response = $this->insertOrder($order);
    	}
        $response->headers->set('Content-Type', 'application/json');
    	return $response;
    }

    /**
     * @param $request
     * @return array Datos del post para el pedido
     */
    private function getPostOrder($request)
    {
    	$order = array();
    	$order["fullName"] = $request->request->get("fullName");
    	$order["email"] = $request->request->get("email");
    	$order["phone"] = $request->request->get("phone");
    	$order["address"] = $request->request->get("address");
    	$order["sentDate"] = $request->request->get("sentDate");
    	$order["deliveryRange"] = $request->request->get("deliveryRange");

    	return $order;
    }

    /**
     * @param $order
     * @return Response JSON con:
     *          status que será 1 o 0 en función del éxito de la operación
     *          Message que da información adicional sobre la inserción
     */
    private function insertOrder($order)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $orderIns = new Order();
        $orderIns->setFullName($order["fullName"]);     
        $orderIns->setEmail($order["email"]);
        $orderIns->setPhone($order["phone"]);
        $orderIns->setAddress($order["address"]);
        $dateTime = new \DateTime();
        $date = $dateTime->createFromFormat("d/m/Y H:i:s", $order["sentDate"]);
        $orderIns->SetSentDate($date);
        $orderIns->setDeliveryRange($order["deliveryRange"]);
        $driver = $this->getRandomDriver();
        $orderIns->setDriver($driver);
        $entityManager->persist($orderIns);

        $response = new Response();
        $resp = "";

        try
        {
            $entityManager->flush();
            $resp = array("status" => 1,
                "Message" => "Pedido creado correctamente");
        }
        catch(\Exception  $e)
        {
            $resp = array("status" => 0,
                            "Message" => "Error al insertar el pedido ". $e->getMessage());
            $response->setStatusCode(500);
        }

        $response->setContent(json_encode($resp));
        return $response;
    }

    /**
     * Obtiene los transportistas y devuelve uno aleatorio
     */
    private function getRandomDriver()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $drivers = $em->getRepository("AppBundle:Driver")->findAll();
        $rand = rand(0,count($drivers)-1);
        return $drivers[$rand];
    }
}