<?php
/**
 * Created by PhpStorm.
 * User: ADRIAN
 * Date: 20/03/2018
 * Time: 19:37
 */

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\Driver;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class DriverController extends Controller
{
    /**
     * @Route("/driver/list/{id}/{date}", name="order_list")
     * @param $id
     * @param $date
     * @return Response
     * Función de toma como parámetro el id del transportista y devuelve los pedidos
     * que tenga asociados en la fecha indicada
     */
    public function getOrderList($id, $date)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $driver = $em->getRepository("AppBundle:Driver")->findDriversByIdAndOrderDate($id,$date);

        $response = new Response();
        if(!$driver)
        {
            $resp = array("status" => 0,
                "orderList" => array());
            $response->setStatusCode(404);
        }
        else
        {
            $resp = array("status" => 0,
                "orderList" => $driver["orders"]);
        }
        $response->setContent(json_encode($resp));
        return $response;
    }
}